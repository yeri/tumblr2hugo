# `_redirect`

This file works with Cloudflare Pages.

Simply permanently redirects tumblr URLs (posts/ID/title/) to Hugo's filename/.

For example https://blog.flatturtle.com/post/78107723960/startupsbe-applied-for-accenturebelux-s-call becomes https://blog.flatturtle.com/78107723960/. 

## Installation

Put it in your static folder of your Hugo installation ¯\\_(ツ)_/¯


## Effect


```
$ curl -I https://blog.flatturtle.com/post/78425342996/flatturtle-in-paris-beta
HTTP/2 301
date: Sun, 07 Jul 2024 00:33:58 GMT
content-type: text/plain;charset=UTF-8
location: /78425342996/
access-control-allow-origin: *
referrer-policy: strict-origin-when-cross-origin
x-content-type-options: nosniff
set-cookie: __cf_bm=xM.Dwkt0vdJfWvA7Z5T6ABwoDsChpmaCi_AcN0dHyz0-1720312438-1.0.1.1-pcZ7Loxno72cR3j2in6KljMRWCJAmtJuhVZz3lnPp8ibo.eowuk7DX1bM8wjFQGBWKgtnxOE1VolBDjt9M66vg; path=/; expires=Sun, 07-Jul-24 01:03:58 GMT; domain=.blog.flatturtle.com; HttpOnly; Secure; SameSite=None
report-to: {"endpoints":[{"url":"https:\/\/a.nel.cloudflare.com\/report\/v4?s=W7fgeRos09YmdG2W9ZdLqyXHaGIRxPCb26VNynN0Qzrxwv14OKt1I%2FWmV%2F6NQEYPktY%2B3Iuq%2F%2FODOZYWB6yW4VutkBrIQPBFkDQAE%2FWKg71BX9k4x8ndGKrQOfU0WcIGe2t129jsSja%2BJT92HsUbm3k5"}],"group":"cf-nel","max_age":604800}
nel: {"success_fraction":0,"report_to":"cf-nel","max_age":604800}
server: cloudflare
cf-ray: 89f3bf054b794a53-SIN
alt-svc: h3=":443"; ma=86400

HTTP/2 200
date: Sun, 07 Jul 2024 00:33:58 GMT
content-type: text/html; charset=utf-8
access-control-allow-origin: *
cache-control: public, max-age=0, must-revalidate
referrer-policy: strict-origin-when-cross-origin
x-content-type-options: nosniff
set-cookie: __cf_bm=r4cOha7_DseEOwvpFtBKGS8KUEfMMq8O.nX9zu3CgoM-1720312438-1.0.1.1-76x2Zp2P8H.Mbj1wxReVKKG0pgJzNfQznfPvGUpm71NRx7_7ux9YFxZDm6DlQLve4GvyXDR3Zj_bLxJOVmUuLQ; path=/; expires=Sun, 07-Jul-24 01:03:58 GMT; domain=.blog.flatturtle.com; HttpOnly; Secure; SameSite=None
report-to: {"endpoints":[{"url":"https:\/\/a.nel.cloudflare.com\/report\/v4?s=AbK25Osi9XggeQTZa6WdZa2elntqZVcpbE6tuvToRqRHHAvrLxq9lQDK3uH4MFVO%2B9bV7orq5aeE%2B9gyz99s2Ayz0n%2BzQ%2Fgvgb7kh%2BfbOoay7EF6hGVLGKtojDIj99QRt%2FksJ6KLitFM9YG5R363g3LA"}],"group":"cf-nel","max_age":604800}
nel: {"success_fraction":0,"report_to":"cf-nel","max_age":604800}
server: cloudflare
cf-ray: 89f3bf057b8b4a53-SIN
alt-svc: h3=":443"; ma=86400
```