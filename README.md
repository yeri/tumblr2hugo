# Tumblr to Hugo 

## Backup

Get an export from your [Tumblr](https://help.tumblr.com/export-your-blog/).

## Edit scripts

Be sure to edit the path and what you want to edit! It's all hardcoded stuff.

## Scripts

1. run `convert_html_to_md.py` to convert the html files to markdown. 
1. run `add_front_matter.py` to add front matter for Hugo. It should automatically take the last line that has the date and convert it.
1. run `update_media_links.py` to update all media links to whatever you want it to be. 