import os
import re

# Directory containing your Markdown files
content_directory = '/Users/yeri/Desktop/hugo/posts'

# Regular expression to match the media links
media_link_regex = re.compile(r'!\[\]\(\.\.\/\.\.\/media\/(.*?)\)')

for filename in os.listdir(content_directory):
	if filename.endswith(".md"):
		filepath = os.path.join(content_directory, filename)
		with open(filepath, 'r', encoding='utf-8') as f:
			content = f.read()

		# Replace media links
		updated_content = media_link_regex.sub(r'![](https://img.flatturtle.com/tumblr/\1)', content)

		# Write the updated content back to the file
		with open(filepath, 'w', encoding='utf-8') as f:
			f.write(updated_content)
			print(f'Updated media links in {filename}')