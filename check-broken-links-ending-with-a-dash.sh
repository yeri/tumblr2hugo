#!/bin/bash

# Simple script that checks links ending with a -.
# We have a lot of MD links that get cut in half and then do not properly get parsed.

# Directory containing the markdown files
MD_DIR="/Users/yeri/git/BlogTurtle/content/posts"

# Function to check if a line ends with a - and not containing ---
check_lines() {
	file=$1
	while IFS= read -r line
	do
		if [[ $line == *- && $line != *---* ]]; then
			echo "File: $file - Line: $line"
		fi
	done < "$file"
}

# Find all .md files in the specified directory and check each one
find "$MD_DIR" -type f -name "*.md" | while IFS= read -r file
do
	check_lines "$file"
done