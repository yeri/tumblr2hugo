import os
import html2text

# Directory containing your HTML files
input_directory = '/Users/yeri/Downloads/b4fed6c62c5fd5dd816ca0264e2a9d54d817cc708e99b8d519158c7c8e677544/posts/html'
output_directory = '/Users/yeri/Desktop/hugo/posts'

# Ensure the output directory exists
os.makedirs(output_directory, exist_ok=True)

# Initialize html2text
h = html2text.HTML2Text()
h.ignore_links = False

for filename in os.listdir(input_directory):
    if filename.endswith(".html"):
        filepath = os.path.join(input_directory, filename)
        with open(filepath, 'r', encoding='utf-8') as f:
            html_content = f.read()
            markdown_content = h.handle(html_content)
            
            # Define the output markdown file path
            markdown_filename = filename.replace('.html', '.md')
            output_filepath = os.path.join(output_directory, markdown_filename)
            
            # Write the markdown content to a new file
            with open(output_filepath, 'w', encoding='utf-8') as md_file:
                md_file.write(markdown_content)
                print(f'Converted {filename} to {markdown_filename}')