import os
import re
from datetime import datetime

# Directory containing your Markdown files
content_directory = '/Users/yeri/Desktop/hugo/posts'

# Regular expressions to match the date and time at the end of the file
date_regex = re.compile(r'(\w+ \d{1,2}(?:st|nd|rd|th)?, \d{4} \d{1,2}:\d{2}(?:am|pm)?)')
front_matter_regex = re.compile(r'(^---\s*\n|^\+\+\+\s*\n)', re.MULTILINE)

# Function to parse the date with various suffixes
def parse_date(date_str):
    for suffix in ['st', 'nd', 'rd', 'th']:
        try:
            date = datetime.strptime(date_str.replace(suffix, ''), '%B %d, %Y %I:%M%p')
            return date.isoformat()
        except ValueError:
            continue
    raise ValueError(f"Date format not recognized: {date_str}")

for filename in os.listdir(content_directory):
    if filename.endswith(".md"):
        filepath = os.path.join(content_directory, filename)
        with open(filepath, 'r', encoding='utf-8') as f:
            content = f.read()

        # Check for existing front matter
        if front_matter_regex.match(content):
            print(f'Skipping {filename} (front matter exists)')
            continue

        # Extract the date and time from the content
        match = date_regex.search(content)
        if match:
            date_str = match.group(1)
            try:
                iso_date = parse_date(date_str)
                content = date_regex.sub('', content).strip()  # Remove the date from the content
            except ValueError as e:
                print(f'Error parsing date in {filename}: {e}')
                continue
        else:
            iso_date = '2024-07-01T00:00:00Z'  # Default date if not found

        # Create front matter
        front_matter = f'''+++
title = "{filename.replace('.md', '').replace('-', ' ').title()}"
date = {iso_date}
draft = false
+++

'''
        # Combine front matter and content
        new_content = front_matter + content

        # Write the new content back to the file
        with open(filepath, 'w', encoding='utf-8') as f:
            f.write(new_content)
            print(f'Added front matter to {filename}')